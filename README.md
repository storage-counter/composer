# composer-repository

This project is intended to test the new GitLab Composer Repository and power a product walk-through. 

# Walk-through

1. Download Composer https://getcomposer.org/download/
1. Create a new GitLab project
1. Create a `composer.json` file and populate with an example package
1. Add a git tag and `push origin --tags`
1. Create a PAT with `api` scope 
1. Copy the `curl` command and update variables
1. Run the `curl` command
1. Check the UI to see if the package has been updated
1. Document any issues as you uncover them


# Issues
1. Composer packages do not have their own tab
1. Composer packages do not include quick install/setup commands
1. UI says packages take up 0 bytes
1. No error message for duplicate uploads
1. When a package is built via pipelines, it still says manually published.